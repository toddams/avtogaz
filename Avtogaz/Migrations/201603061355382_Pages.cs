namespace Avtogaz.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Pages : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PageModels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Alias = c.String(nullable: false),
                        Name = c.String(nullable: false),
                        Content = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.PageModels");
        }
    }
}
