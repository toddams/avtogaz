﻿using System.Collections.Generic;
using Avtogaz.Controllers;
using Avtogaz.Models;

namespace Avtogaz.Data
{
	public class GalleryViewModel
	{
		public ICollection<GalleryCategory> Categories { get; set; }

		public ICollection<GalleryPreview> Previews { get; set; }
	}

	public class GalleryPreview
	{
		public int Id { get; set; }

		public string Title { get; set; }

		public string Image { get; set; }

		public string Mark { get; set; }

		public string Model { get; set; }

		public short Year { get; set; }

		public string EngineCapacity { get; set; }

		public byte CylinderCount { get; set; }

		public string Power { get; set; }

		public string EngineType { get; set; }
	}
}