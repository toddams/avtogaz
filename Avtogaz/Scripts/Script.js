﻿var loadingDiv = $('.loader');
$(document).ajaxStart(function () {
    loadingDiv.show();
});

$(document).ajaxStop(function () {
    loadingDiv.hide();
});

$(function() {
    $(".preview-gallery-item").click(function() {
        var href = $(this).data("href");

        window.location.href = href;
    });
})