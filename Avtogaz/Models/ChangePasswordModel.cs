﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Avtogaz.Models
{
	public class ChangePasswordModel
	{
		[Required]
		public string OldPassword { get; set; }

		[Required]
		public string NewPassword { get; set; }

		[Required]
		[MinLength(6)]
		[Compare("NewPassword", ErrorMessage = "Введені паролі не впівпадають")]
		public string ConfirmPassword { get; set; }
	}
}