﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Avtogaz.Models
{
	public class HomeViewModel
	{
		public List<ArticleModel> Articles { get; set; }
		public List<GalleryHomePreview> GalleryPreviews { get; set; }
	}

	public class GalleryHomePreview
	{
		public string Image { get; set; }

		public int Id { get; set; }
	}
}