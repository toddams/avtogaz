﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Avtogaz.Models
{
	public class PageModel
	{
		[Key]
		public int Id { get; set; }

		[Required]
		public string Alias { get; set; }

		[Required]
		public string Name { get; set; }

		[Required]
		[AllowHtml]
		public string Content { get; set; }
	}
}