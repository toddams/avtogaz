﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Avtogaz.Models
{
	[Table("Articles")]
	public class ArticleModel
	{
		[Key]
		public int Id { get; set; }

		[Required, MinLength(3)]
		public string Title { get; set; }

		[AllowHtml]
		public string Cut { get; set; }

		[AllowHtml]
		[Required, MinLength(20)]
		public string Text { get; set; }

		public DateTime PostTime { get; set; }
	}
}