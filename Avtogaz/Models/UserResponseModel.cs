﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Avtogaz.Models
{
	[Table("UserResponses")]
	public class UserResponseModel
	{
		[Key]
		public int Id { get; set; }

		[Required]
		public string Name { get; set; }

		[Required, EmailAddress]
		public string Email { get; set; }

		[Required, MinLength(10), MaxLength(1000)]
		public string Text { get; set; }

		public DateTime Date { get; set; }

		public bool IsRead { get; set; }

		public bool IsDeleted { get; set; }
	}
}