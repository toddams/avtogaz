﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web;
using System.Web.Mvc;

namespace Avtogaz.Models
{
	public class GalleryCategory
	{
		[Key]
		public int Id { get; set; }

		[Required]
		public string Name { get; set; }

		public virtual ICollection<GalleryItem> Items { get; set; }
	}

	public class GalleryItem
	{
		[Key]
		public int Id { get; set; }
		
		[Required]
		public string Title { get; set; }

		[Required]
		public string Mark { get; set; }

		[Required]
		public string Model { get; set; }

		public short Year { get; set; }

		[Required]
		public string EngineCapacity { get; set; }

		[Required]
		public byte CylinderCount { get; set; }

		[Required]
		public string Power { get; set; }

		[Required]
		public string EngineType { get; set; }


		[AllowHtml]
		[Required]
		public string Text { get; set; }

		public DateTime PostTime { get; set; }

		public int CategoryId { get; set; }
		public GalleryCategory Category { get; set; }

		public string Image { get; set; }

		[NotMapped]
		public HttpPostedFileBase File { get; set; }
		[NotMapped]
		public IEnumerable<SelectListItem> Categories { get; set; } 
		[NotMapped]
		public IEnumerable<SelectListItem> EngineTypes { get; set; } 
	}

}