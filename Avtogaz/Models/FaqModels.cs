﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Avtogaz.Models
{
	public class FaqCateogry
	{
		[Key]
		public int Id { get; set; }

		[Required]
		public string Title { get; set; }

		public virtual ICollection<FaqItem> Faqs { get; set; } 
	}

	public class FaqItem
	{
		[Key]
		public int Id { get; set; }

		[Required]
		public string Title { get; set; }

		[Required]
		public string Text { get; set; }

		public bool ShowInSideBlock { get; set; }

		public int CategoryId { get; set; }
		public virtual FaqCateogry Category { get; set; }
	}

	public class FaqViewModel
	{
		public List<FaqItem> Items { get; set; }

		public IEnumerable<SelectListItem> Categories { get; set; }

		public int CategoryId { get; set; }
	}
}