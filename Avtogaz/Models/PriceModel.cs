﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Avtogaz.Models
{
	[Table("Prices")]
	public class PriceModel
	{
		[Key]
		public int Id { get; set; }

		public DateTime ChangeDate { get; set; }

		public string Carburator1 { get; set; }

		public string Carburator2 { get; set; }

		public string MonoInjector1 { get; set; }

		public string MonoInjector2 { get; set; }

		public string MechInjector1 { get; set; }

		public string MechInjector2 { get; set; }

		public string ElectroInjector1 { get; set; }

		public string ElectroInjector2 { get; set; }

		public string SpecialAveo1 { get; set; }

		public string SpecialAveo2 { get; set; }

		public string Cilinder4 { get; set; }

		public string Cilinder6 { get; set; }

		public string Cilinder8 { get; set; }
	}
}