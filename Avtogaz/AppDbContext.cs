﻿using System.Data.Entity;
using Avtogaz.Data;
using Avtogaz.Models;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Avtogaz
{
	public class AppDbContext : IdentityDbContext<AppUser>
	{
		public AppDbContext() : base("ExologConnection")
		{
		}

		public DbSet<UserResponseModel> UserResponses { get; set; }
		public DbSet<PriceModel> Prices { get; set; }
		public DbSet<ArticleModel> Articles { get; set; }

		public DbSet<GalleryCategory> GalleryCategories { get; set; }
		public DbSet<GalleryItem> GalleryItems { get; set; }

		public DbSet<FaqCateogry> FaqCateogries { get; set; }
		public DbSet<FaqItem> FaqItems { get; set; }

		public DbSet<PageModel> Pages { get; set; }
	}
}