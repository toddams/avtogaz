﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Avtogaz.Attributes;
using Avtogaz.Data;
using Avtogaz.Models;
using Microsoft.AspNet.Identity;
using Constants = Avtogaz.Helpers.Constants;

namespace Avtogaz.Controllers
{
    public class HomeController : Controller
    {
	    private readonly AppDbContext db;

	    public HomeController()
	    {
		    db = new AppDbContext();
	    }

        // GET: Home
        public async Task<ActionResult> Index()
        {
	        List<GalleryHomePreview> galleryPreviews = await db.GalleryItems.
		        OrderByDescending(e => e.PostTime).
		        Take(10).
		        Select(e => new GalleryHomePreview()
		        {
			        Id = e.Id,
					Image = e.Image
				}).
		        ToListAsync();

	        foreach (var g in galleryPreviews)
		        g.Image = $"{Constants.GALLERY_PREVIEWS_PATH}/{g.Image}";


			List<ArticleModel> articles = await db.Articles.
		        OrderByDescending(a => a.PostTime).
		        Take(4).
		        ToListAsync();

			var homeViewModel = new HomeViewModel()
	        {
				Articles = articles,
				GalleryPreviews = galleryPreviews
	        };

            return View(homeViewModel);
        }

	    public ActionResult Prices()
	    {
			PriceModel model = db.Prices.OrderByDescending(m => m.ChangeDate).Take(1).SingleOrDefault();

			return View(model);
	    }

	    public ActionResult FAQ()
	    {
		    var a = from i in db.FaqItems
			    group i by i.Category.Title
			    into list
			    select list;

		    return View(a.ToList());
	    }

		[HttpGet]
	    public ActionResult Contacts()
		{
			if (TempData["message"] != null)
				ViewBag.Message = TempData["message"];

			return View();
	    }
		
		[HttpPost]
		[ValidateModelState]
	    public async Task<ActionResult> Contacts(UserResponseModel model)
		{
			model.Date = DateTime.Now;

			try
			{
				db.UserResponses.Add(model);
				await db.SaveChangesAsync();
			}
			catch (Exception ex)
			{
				// Ignore
			}

			TempData["message"] = "Дякуємо за ваше повідомлення, ми звяжемось з вами найближчим часом";
			return RedirectToAction("Contacts");
		}

	    public ActionResult Create()
	    {
			db.Database.Initialize(true);
		    var user = new AppUser()
		    {
			    Email = "toddams@gmail.com",
			    UserName = "toddams@gmail.com",
		    };

		    Startup.UserManagerFactory().Create(user, "malina123");
		    return Content("Created");
	    }

	    public ActionResult AboutUs()
	    {
		    return View();
	    }

	    public ActionResult Services()
	    {
		    return View();
	    }

	    public ActionResult Benefits()
	    {
		    return View();
	    }

	    public ActionResult Credit()
	    {
		    return View();
	    }
    }
}