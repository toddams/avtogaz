﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Xml;
using System.Xml.XPath;
using Avtogaz.Attributes;
using Avtogaz.Data;
using Avtogaz.Helpers;
using Avtogaz.Models;

using Microsoft.AspNet.Identity;

namespace Avtogaz.Controllers
{
	[Authorize]
	public class AdminController : Controller
	{
		private readonly UserManager<AppUser> userManager;
		private readonly AppDbContext db;

		//Dashboard
		public ActionResult Index()
		{
			return View();
		}

		#region "Constructors"
		public AdminController() : this(Startup.UserManagerFactory.Invoke())
		{
			db = new AppDbContext();
		}

		public AdminController(UserManager<AppUser> userManager)
		{
			this.userManager = userManager;
		}
		#endregion

		#region "Login"

		[AllowAnonymous]
		public ActionResult Login()
		{
			return View();
		}

		[ValidateModelState]
		[ValidateAntiForgeryToken]
		[HttpPost, AllowAnonymous]
		public async Task<ActionResult> Login(LoginModel model)
		{
			var user = await userManager.FindAsync(model.Email, model.Password);

			if (user != null)
			{
				var identity = await userManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
				HttpContext.GetOwinContext().Authentication.SignIn(identity);

				return RedirectToAction("Index");
			}

			ModelState.AddModelError("", "Введені вами логін або пароль невірні");
			return View(model);
		}

		#endregion

		#region "Change password"

		//Change password
		public ActionResult ChangePassword(string message)
		{
			ViewBag.Message = message;
			return View();
		}

		[HttpPost]
		[ValidateModelState]
		[ValidateAntiForgeryToken]
		public async Task<ActionResult> ChangePassword(ChangePasswordModel model)
		{
			if (model.OldPassword == model.NewPassword)
			{
				ModelState.AddModelError("", "Старий пароль повинен відрізнятися від нового");
				return View(model);
			}

			string userId = User.Identity.GetUserId();
			AppUser user = await userManager.FindByIdAsync(userId);
			if (user == null)
			{
				ModelState.AddModelError("", "Даного користувача не знайдено");
				return View(model);
			}

			IdentityResult result = await userManager.ChangePasswordAsync(userId, model.OldPassword, model.NewPassword);
			if (!result.Succeeded)
			{
				ModelState.AddModelError("", result.Errors.FirstOrDefault());
				return View(model);
			}

			return RedirectToAction("ChangePassword", new { message = "Пароль успішно змінено" });
		}

		#endregion

		#region "Responses"

		[HttpGet]
		public ActionResult Responses()
		{
			var model = db.UserResponses.Where(r => !r.IsDeleted).ToList();

			return View(model);
		}

		[HttpGet]
		public async Task<ActionResult> ReadResponse(int? id)
		{
			if (!id.HasValue)
				return RedirectToAction("Responses");

			UserResponseModel response = await db.UserResponses.FindAsync(id.Value);
			response.IsRead = true;
			await db.SaveChangesAsync();

			return View(response);
		}

		[HttpPost]
		public JsonResult DeleteResponse(int id)
		{
			var response = db.UserResponses.Find(id);
			if (response == null)
				return Json(new { success = "false" });

			response.IsDeleted = true;
			db.SaveChanges();

			return Json(new { success = "true" });
		}

		#endregion

		#region "Prices"

		[HttpGet]
		public ActionResult Prices()
		{
			PriceModel model = db.Prices.OrderByDescending(m => m.ChangeDate).Take(1).SingleOrDefault();
			if (TempData["message"] != null)
				ViewBag.Message = TempData["message"];

			return View(model);
		}

		[ValidateModelState]
		public ActionResult Prices(PriceModel model)
		{
			model.ChangeDate = DateTime.Now;

			db.Prices.Add(model);
			db.SaveChanges();

			TempData["message"] = "success";
			return RedirectToAction("Prices");
		}

		#endregion

		#region "Articles"

		public ActionResult Articles()
		{
			var model = db.Articles.ToList();
			return View(model);
		}

		[HttpGet]
		public ActionResult NewArticle()
		{
			if (TempData["message"] != null)
				ViewBag.Message = TempData["message"];

			return View();
		}

		[HttpPost]
		[ValidateModelState]
		public ActionResult NewArticle(ArticleModel model)
		{
			model.Cut = model.Text.TruncateHtml(600, "...");
			model.PostTime = DateTime.Now;

			db.Articles.Add(model);
			db.SaveChanges();

			TempData["message"] = "Ваша стаття успішно опублікована";
			return RedirectToAction("NewArticle");
		}

		[HttpPost]
		public ActionResult DeleteArticle(int id)
		{
			if (id == 0)
				return HttpNotFound();

			var article = db.Articles.SingleOrDefault(a => a.Id == id);
			if (article == null)
				return Json(new { success = false});

			try
			{
				db.Articles.Remove(article);
				db.SaveChanges();
			}
			catch (Exception)
			{
				return Json(new { success = false });
			}

			return Json(new { success = true });
		}

		public ActionResult EditArticle(int id)
		{
			if (TempData["message"] != null)
				ViewBag.Message = TempData["message"];

			var article = db.Articles.SingleOrDefault(e => e.Id == id);
			if(article == null)
				return HttpNotFound();

			return View(article);
		}

		[HttpPost]
		public ActionResult EditArticle(ArticleModel article)
		{
			var art = db.Articles.Find(article.Id);
			if (art == null)
				return HttpNotFound();

			art.Title = article.Title;
			art.Text = article.Text;
			art.Cut = article.Text.TruncateHtml(600, "...");
			art.PostTime = DateTime.Now;

			db.Entry(art).State = EntityState.Modified;
			db.SaveChanges();

			TempData["message"] = "Стаття успішно відредагована";
			return RedirectToAction("EditArticle", new {Id = article.Id});
		}

		#endregion

		#region Gallery

		public ActionResult Galleries()
		{
			var galleries = db.GalleryItems.ToList();

			return View(galleries);
		}

		public ActionResult EditGallery(int id)
		{
			var model = db.GalleryItems.SingleOrDefault(g => g.Id == id);
			if (model == null)
				return HttpNotFound();

			var categories = db.GalleryCategories.Select(c => new SelectListItem()
			{
				Text = c.Name,
				Value = c.Id.ToString()
			}).ToList();

			var engineTypes = new List<SelectListItem>()
			{
				new SelectListItem() {Text = "Розподілений вприск", Value = "Розподілений вприск"},
				new SelectListItem() {Text = "Безпосередній вприск", Value = "Безпосередній вприск"}
			};

			model.EngineTypes = engineTypes;
			model.Categories = categories;

			return View(model);
		}

		[HttpPost]
		public ActionResult EditGallery(GalleryItem item)
		{
			if (item == null)
				return HttpNotFound();

			var gallery = db.GalleryItems.SingleOrDefault(g => g.Id == item.Id);
			if (gallery == null)
				return HttpNotFound();

			gallery.Title = item.Title;
			gallery.Text = item.Text;
			gallery.CategoryId = item.CategoryId;

			gallery.Mark = item.Mark;
			gallery.Model = item.Model;
			gallery.Year = item.Year;
			gallery.EngineType = item.EngineType;
			gallery.EngineCapacity = item.EngineCapacity;
			gallery.CylinderCount = item.CylinderCount;
			gallery.Power = item.Power;


			if (item.File?.ContentLength > 0)
			{
				ImageResizer.ImageJob i = new ImageResizer.ImageJob(item.File, "~/uploads/gallery_previews/<guid>.<ext>", new
					ImageResizer.ResizeSettings("width=220;height=130;format=jpg;mode=max"));
				i.CreateParentDirectory = true;
				i.Build();
				gallery.Image = System.IO.Path.GetFileName(i.FinalPath);
			}

			db.SaveChanges();

			return RedirectToAction("Galleries");
		}

		[HttpGet]
		public ActionResult NewGallery()
		{
			if (TempData["Message"] != null)
				ViewBag.Message = TempData["Message"];

			var categories = db.GalleryCategories.Select(c => new SelectListItem()
			{
				Text = c.Name,
				Value = c.Id.ToString()
			}).ToList();

			var engineTypes = new List<SelectListItem>()
			{
				new SelectListItem() {Text = "Розподілений вприск", Value = "Розподілений вприск"},
				new SelectListItem() {Text = "Безпосередній вприск", Value = "Безпосередній вприск"}
			};


			var model = new GalleryItem() { Categories = categories, EngineTypes = engineTypes};


			return View(model);
		}

		[HttpPost]
		public ActionResult NewGallery(GalleryItem item)
		{
			try
			{
				if (!ModelState.IsValid)
					return RedirectToAction("NewGallery");

				if (item.File.ContentLength <= 0)
					return null;
				ImageResizer.ImageJob i = new ImageResizer.ImageJob(item.File, "~/uploads/gallery_previews/<guid>.<ext>", new
					ImageResizer.ResizeSettings("width=220;height=130;format=jpg;mode=max"));

				i.CreateParentDirectory = true;
				i.Build();

				item.Image = System.IO.Path.GetFileName(i.FinalPath);
				item.PostTime = DateTime.Now;
				db.GalleryItems.Add(item);
				db.SaveChanges();

				//TODO: add categories

				TempData["Message"] = "Ваша публікація успішно опублікована";
				return RedirectToAction("NewGallery");
			}
			catch (Exception ex)
			{
				ModelState.AddModelError("", ex.Message);
				return View(item);
			}
		}

		[HttpGet]
		public ActionResult GalleryCategories()
		{
			var model = db.GalleryCategories.ToList();
			return View(model);
		}

		[HttpPost]
		public ActionResult GalleryCategories(GalleryCategory category)
		{
			try
			{
				db.GalleryCategories.Add(category);
				db.SaveChanges();
			}
			catch (Exception ex)
			{
				ModelState.AddModelError("", ex.Message);
			}

			var model = db.GalleryCategories.ToList();
			return View(model);
		}

		public JsonResult RemoveGalleryCategory(int id)
		{
			try
			{
				var category = db.GalleryCategories.SingleOrDefault(c => c.Id == id);
				if (category == null)
					throw new Exception("Категорія з таким ID не знайдена");
				if (category.Items.Count > 0)
					throw new Exception("Неможливо видалити непусту категорію");

				db.GalleryCategories.Remove(category);
				db.SaveChanges();

				return Json(new { Success = true, Id = id });
			}
			catch (Exception ex)
			{
				return Json(new { Success = false, Message = ex.Message });
			}
		}
		#endregion

		#region FAQ

		[HttpGet]
		public ActionResult FaqCategories()
		{
			List<FaqCateogry> categories = db.FaqCateogries.ToList();

			return View(categories);
		}

		[HttpPost, ValidateModelState]
		public ActionResult FaqCategories(FaqCateogry category)
		{
			try
			{
				db.FaqCateogries.Add(category);
				db.SaveChanges();
			}
			catch (Exception ex)
			{
				ModelState.AddModelError("", ex.Message);
			}

			var model = db.FaqCateogries.ToList();
			return View(model);
		}

		[HttpGet]
		public ActionResult Faq()
		{
			List<FaqItem> faqs = db.FaqItems.Include(f => f.Category).ToList();
			var categories = db.FaqCateogries.Select(c => new SelectListItem()
			{
				Text = c.Title,
				Value = c.Id.ToString()
			});

			var model = new FaqViewModel() {Categories = categories, Items = faqs};
			
			return View(model);
		}

		[HttpPost, ValidateModelState]
		public ActionResult AddFaq(FaqItem faq)
		{
			try
			{
				db.FaqItems.Add(faq);
				db.SaveChanges();
			}
			catch (Exception)
			{
				//				
			}

			return RedirectToAction("Faq");
		}

		[HttpPost]
		public JsonResult DeleteFaqCategory(int categoryId)
		{
			try
			{
				var category = db.FaqCateogries.SingleOrDefault(f => f.Id == categoryId);
				if (category == null)
					throw new Exception("Категорія з таким ID не знайдена");

				if (category.Faqs.Count > 0)
					throw new Exception("Неможливо видалити непусту категорію");

				db.FaqCateogries.Remove(category);
				db.SaveChanges();

				return Json(new { Success = true, Id = categoryId });
			}
			catch (Exception ex)
			{
				return Json(new { Success = false, Message = ex.Message });
			}
		}

		[HttpPost]
		public JsonResult DeleteFaqItem(int id)
		{
			try
			{
				var faq = db.FaqItems.SingleOrDefault(f => f.Id == id);
				if (faq == null)
					throw new Exception("FAQ з таким ID не знайдений");

				db.FaqItems.Remove(faq);
				db.SaveChanges();

				return Json(new { Success = true, Id = id });
			}
			catch (Exception ex)
			{
				return Json(new { Success = false, Message = ex.Message });
			}
		}
		#endregion

		public ActionResult LogOut()
		{
			HttpContext.GetOwinContext().Authentication.SignOut("ApplicationCookie");
			return RedirectToAction("Login");
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && userManager != null)
			{
				userManager.Dispose();
			}

			base.Dispose(disposing);
		}
	}
}