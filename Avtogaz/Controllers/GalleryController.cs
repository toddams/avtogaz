﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Avtogaz.Data;
using Avtogaz.Helpers;
using Avtogaz.Models;

namespace Avtogaz.Controllers
{
	public class GalleryController : Controller
	{
		public AppDbContext db { get; set; }

		public GalleryController()
		{
			db = new AppDbContext();
		}

		// GET: Gallery
		public ActionResult Index(int? category)
		{
			var previews = new List<GalleryPreview>();

			if (category.HasValue)
			{
				previews = db.GalleryItems.AsNoTracking().
					Where(g => g.CategoryId == category.Value).
					AsEnumerable().
					Select(g => new GalleryPreview()
					{
						Id = g.Id,
						Title = g.Title,
						Model = g.Model,
						CylinderCount = g.CylinderCount,
						EngineCapacity = g.EngineCapacity,
						EngineType = g.EngineType,
						Mark = g.Mark,
						Power = g.Power,
						Year = g.Year,
						Image = System.IO.Path.Combine(Constants.GALLERY_PREVIEWS_PATH, g.Image)
					}).ToList();
			}
			else
			{
				previews = db.GalleryItems.AsNoTracking().
					AsEnumerable().
					Select(g => new GalleryPreview()
					{
						Id = g.Id,
						Title = g.Title,
						Model = g.Model,
						CylinderCount = g.CylinderCount,
						EngineCapacity = g.EngineCapacity,
						EngineType = g.EngineType,
						Mark = g.Mark,
						Power = g.Power,
						Year = g.Year,
						Image = System.IO.Path.Combine(Constants.GALLERY_PREVIEWS_PATH, g.Image)
					}).ToList();
			}


			//TODO: add image

			var categories = db.GalleryCategories.ToList();
			var model = new GalleryViewModel()
			{
				Categories = categories,
				Previews = previews
			};

			return View(model);
		}
	
		public ActionResult Job(int? id)
		{
			if(!id.HasValue)
				return HttpNotFound();

			GalleryItem job = db.GalleryItems.SingleOrDefault(j => j.Id == id.Value);

			return View(job);
		}
	}
}