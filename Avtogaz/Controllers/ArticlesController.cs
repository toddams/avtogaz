﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Avtogaz.Controllers
{
    public class ArticlesController : Controller
    {
	    private readonly AppDbContext db;

	    public ArticlesController()
	    {
		    this.db = new AppDbContext();
	    }

	    // GET: Articles
        public async Task<ActionResult> Index()
        {
	        var articles = await db.Articles.OrderByDescending(a => a.PostTime).ToListAsync();

			return View(articles);
        }

	    public async Task<ActionResult> Article(int id = 1)
	    {
		    var article = await db.Articles.FindAsync(id);
		    if (article == null)
			    return HttpNotFound();

		    return View(article);
	    }
    }
}