﻿using System.Data.Entity;

using Avtogaz.Data;
using Microsoft.AspNet.Identity;

namespace Avtogaz
{
	public class DbInitializer : DropCreateDatabaseIfModelChanges<AppDbContext>
	{
		protected override void Seed(AppDbContext context)
		{
			var user = new AppUser() {Email = "toddams@gmail.com", UserName = "toddams@gmail.com"};

			base.Seed(context);
		}
	}
}