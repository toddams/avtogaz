﻿using System;
using Avtogaz.Data;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;

[assembly: OwinStartup(typeof(Avtogaz.Startup))]

namespace Avtogaz
{
	public class Startup
	{
		public static Func<UserManager<AppUser>> UserManagerFactory { get; private set; }

		public void Configuration(IAppBuilder app)
		{
			app.UseCookieAuthentication(new CookieAuthenticationOptions
			{
				AuthenticationType = "ApplicationCookie",
				LoginPath = new PathString("/admin/login"),
				CookieName = "auth"
			});

			UserManagerFactory = () =>
			{
				var usermanager = new UserManager<AppUser>(new UserStore<AppUser>(new AppDbContext()));

				usermanager.UserValidator = new UserValidator<AppUser>(usermanager)
				{
					AllowOnlyAlphanumericUserNames = true,
					RequireUniqueEmail = true
				};

				return usermanager;
			};
		}
	}
}
