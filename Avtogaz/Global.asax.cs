﻿using System.Data.Entity;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Avtogaz
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
			FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);

	        Database.SetInitializer<AppDbContext>(null);
        }
    }
}
