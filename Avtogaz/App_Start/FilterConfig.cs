﻿using System.Web.Mvc;
using Avtogaz.App_Start;

namespace Avtogaz
{
	public class FilterConfig
	{
		public static void RegisterGlobalFilters(GlobalFilterCollection filters)
		{
			filters.Add(new HandleErrorAttribute());
			filters.Add(new OnActionExecutingFilter());
		}
	}
}