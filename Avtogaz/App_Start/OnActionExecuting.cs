﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Avtogaz.Models;

namespace Avtogaz.App_Start
{
	public class OnActionExecutingFilter : ActionFilterAttribute
	{
		public override void OnActionExecuting(ActionExecutingContext filterContext)
		{
			using (var db = new AppDbContext())
			{
				var faqs = db.FaqItems.Where(e => e.ShowInSideBlock).ToList();
				if (faqs.Count > 0)
					filterContext.Controller.ViewBag.Faqs = faqs;
				else
					filterContext.Controller.ViewBag.Faqs = new List<FaqItem>();
			}

			base.OnActionExecuting(filterContext);
		}
	}
}